//
//  ViewController.m
//  testbon
//
//  Created by admin on 6/20/2558 BE.
//  Copyright (c) 2558 admin. All rights reserved.
//

#import "ViewController.h"
#import <arpa/inet.h>

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    // Do any additional setup after loading the view.
    
    NSLog(@"test logging");
    
    
    id delegateObject = self; // Assume this exists.
    
    serviceBrowser = [[NSNetServiceBrowser alloc] init];
    [serviceBrowser setDelegate:delegateObject];
    [serviceBrowser searchForServicesOfType:@"_test._tcp" inDomain:@""];

}


- (void)netServiceBrowser:(NSNetServiceBrowser *)aNetServiceBrowser didFindService:(NSNetService *)aNetService moreComing:(BOOL)moreComing {
    NSLog(@"Found %@", [aNetService description]);
    NSLog(@"Found! %@ name(%@) type(%@) domain(%@) address[%lu]", [aNetService hostName], [aNetService name], [aNetService type], [aNetService domain], (unsigned long)[[aNetService addresses] count]);
    
    
    theService = aNetService;
    aNetService.delegate = self;
    [aNetService resolveWithTimeout:5];
}

- (void)setRepresentedObject:(id)representedObject {
    [super setRepresentedObject:representedObject];

    // Update the view, if already loaded.
}

- (void)netServiceDidResolveAddress:(NSNetService *)sender {
    
    NSString *name;
    
    NSData             *address = nil;
    struct sockaddr_in *socketAddress = nil;
    NSString           *ipString = nil;
    int                port;
    
    int i;
    
    for (i=0; i<[sender addresses].count; i++) {
        address = [sender addresses][i];
        
        name = [sender name];
        address = [[sender addresses] objectAtIndex: i];
        socketAddress = (struct sockaddr_in *) [address bytes];

        char s[INET_ADDRSTRLEN];
        inet_ntop(AF_INET, &(socketAddress->sin_addr), s, INET_ADDRSTRLEN);
        ipString = [NSString stringWithFormat:@"%s", s];
        
        port = socketAddress->sin_port;
        
        
        NSString *debugString = [NSString stringWithFormat:@"Resolved: %@-->%@:%d\n", [sender hostName], ipString, port];
        NSLog(@"%@\n", debugString);
    }
    
    
}

@end
